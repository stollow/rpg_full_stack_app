import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeroesListLayoutComponent } from './heroes-list-layout/heroes-list-layout.component';
import {HeroesService} from './services/heroes.service';
import {HttpClientModule} from '@angular/common/http';
import { HeroeCardComponent } from './heroe-card/heroe-card.component';
import { NavbarComponent } from './utils/navbar/navbar.component';
import { HeroFormComponent } from './hero-form/hero-form.component';
import { RouterModule, Routes } from '@angular/router';
import { HeroLayoutComponent } from './hero-layout/hero-layout.component';

const appRoutes: Routes = [
  { path: '', component: HeroesListLayoutComponent },
  { path: 'hero/:id', component: HeroLayoutComponent },
  { path: 'hero/create',      component: HeroFormComponent },
];

@NgModule({
  declarations: [
    AppComponent,
    HeroesListLayoutComponent,
    HeroeCardComponent,
    NavbarComponent,
    HeroFormComponent,
    HeroLayoutComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [
    HeroesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
