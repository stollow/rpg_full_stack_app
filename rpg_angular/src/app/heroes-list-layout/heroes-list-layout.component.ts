import { Component, OnInit } from '@angular/core';
import {HeroesService} from '../services/heroes.service';
import {HeroesModel} from '../models/heroes';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'heroes-list-layout',
  templateUrl: './heroes-list-layout.component.html',
  styleUrls: ['./heroes-list-layout.component.css']
})
export class HeroesListLayoutComponent implements OnInit {

  heroes: HeroesModel[];

  constructor(private heroesService: HeroesService) { }

  ngOnInit() {
    this.heroesService.getHeroes().subscribe(response => {
      this.heroes = response;
    });
  }

}
