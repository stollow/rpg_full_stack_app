import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {HeroesService} from '../services/heroes.service';
import {HeroesModel} from '../models/heroes';
import {switchMap} from 'rxjs/operators';


@Component({
  selector: 'hero-layout',
  templateUrl: './hero-layout.component.html',
  styleUrls: ['./hero-layout.component.css']
})
export class HeroLayoutComponent implements OnInit {

  hero: HeroesModel;
  loading = true;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: HeroesService
  ) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.service.getHero(id)
      .subscribe( response => {
      this.hero = response;
      this.loading = false;
    });
  }

}
