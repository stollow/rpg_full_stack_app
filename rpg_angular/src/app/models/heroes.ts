import {StatsModel} from './stats';
import {WeaponSet} from './weapon_set';
import {ArmorConfig} from './armor_config';

export class HeroesModel {
  id: string;
  name: string;
  sexe: string;
  race: string;
  gold: number;
  stats: StatsModel;
  armorConfig: ArmorConfig;
  config: WeaponSet[];


  constructor() {
  }

}
