export class Armor {
  weigth: number;
  armor: number;
  material: string;
  state: number;
  name: string;
  piece: string;


  constructor(weigth: number, armor: number, material: string, state: number, name: string, piece: string) {
    this.weigth = weigth;
    this.armor = armor;
    this.material = material;
    this.state = state;
    this.name = name;
    this.piece = piece;
  }
}
