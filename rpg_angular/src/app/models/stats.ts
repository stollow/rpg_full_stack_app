export class StatsModel {
  intelligence: number;
  luck: number;
  agility: number;
  vigor: number;
  strength: number;

  constructor() {
  }

}
