import {Component, Input, OnInit} from '@angular/core';
import {HeroesModel} from '../models/heroes';
import {weaponsTypes} from '../enums/weapons-types-enum';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import {HeroesService} from '../services/heroes.service';

@Component({
  selector: 'heroe-card',
  templateUrl: './heroe-card.component.html',
  styleUrls: ['./heroe-card.component.css']
})
export class HeroeCardComponent implements OnInit {

  @Input() hero: HeroesModel;
  life = 0;
  totalArmor = 0;
  stats = [];
  armors = [];
  type = weaponsTypes;
  setBonus = true;
  MapStats = {Intelligence: {name: 'ra ra-book', color: 'blue', bonus: undefined},
    Luck: {name: 'ra ra-perspective-dice-random', color: 'black', bonus: undefined},
    Agility: {name: 'ra ra-player-dodge', color: '#8FBC8F', bonus: undefined},
    Vigor: {name: 'ra ra-hearts', color: 'green', bonus: undefined},
    Strength: {name: 'ra ra-muscle-up', color: 'red', bonus: undefined
    }};

  constructor(private router: Router) { }

  ngOnInit() {
        Object.keys(this.hero.stats).forEach((key: string) => {
        this.stats.push({name: key , value: this.hero.stats[key]});
      });
        let material = null;
        this.hero.armorConfig.armorsPieces.forEach(armor => {
          if (material === null) {
            material = armor.material;
          }
          if (armor.material !== material && material === true) {
            this.setBonus = false;
          }
          this.totalArmor += armor.armor;
          switch (armor.piece.toLowerCase()) {
            case 'helmet' :
              this.armors.push({piece: armor, icon: 'ra ra-helmet', font: this.getColor(armor.material)});
              break;
            case 'chestplate' :
              this.armors.push({piece: armor, icon: 'ra ra-vest', font: this.getColor(armor.material)});
              break;
            case 'boots' :
              this.armors.push({piece: armor, icon: 'ra ra-boot-stomp', font: this.getColor(armor.material)});
              break;
            case 'gauntlet' :
              this.armors.push({piece: armor, icon: 'ra ra-hand', font: this.getColor(armor.material)});
              break;
          }
        }
      );
        this.life = (this.stats.filter(stat =>
        stat.name === 'Strength')[0].value + (this.MapStats.Strength.bonus || 0)) * 3;
  }

  getColor(material: string) {
    // tslint:disable-next-line:no-shadowed-variable
    let armorIconColor;
    switch (material) {
      case 'silk':
        this.applyBonus('Intelligence', 20);
        armorIconColor  = {colorStyle : '#FF00F7'};
        break;
      case 'leather':
        this.applyBonus('Agility', 10);
        this.applyBonus('Vigor', 10);
        armorIconColor = {colorStyle : '#A46800'};
        break;
      case 'cotton':
        this.applyBonus('Agility', 20);
        armorIconColor = {colorStyle : '#AFAEAD'};
        break;
      case 'chainmail':
        this.applyBonus('Strength', 10);
        this.applyBonus('Vigor', 10);
        armorIconColor = {colorStyle : '#7C7C7C'};
        break;
      case 'plate':
        this.applyBonus('Strength', 20);
        armorIconColor = {colorStyle : 'black'};
        break;
    }
    return armorIconColor;
  }

  applyBonus(stat: string, value: number) {
    this.MapStats[stat].bonus = value;
  }

  getIconColor(y: number) {
    const x = y * 255;
    // tslint:disable-next-line:no-shadowed-variable
    let colors;
    if (x > 125) {
      const redColor: number = 505 -  2 * x;
      colors = {
        r: redColor,
        g: 255
      };
    } else {
      const greenColor: number = x;
      colors = {
        r: 255,
        g: greenColor
      };
    }
    return colors;
  }

  gotoDetails(hero: HeroesModel) {
    this.router.navigate(['/hero/', hero.id]);
  }

}
