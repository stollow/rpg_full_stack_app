export const weaponsTypes = {
    BOW: {icon: 'ra  ra-archer', french_name: 'Arc'},
    CROSSBOW: {icon: 'ra   ra-crossbow', french_name: 'Arbalète'},
    ONE_HANDED_SWORD: {icon: 'ra  ra-sword', french_name: 'épée'},
    DAGGER: {icon: 'ra   ra-bowie-knife', french_name: 'Dague'},
    TWO_HANDED_SWORD: {icon: 'ra  ra-croc-sword', french_name: 'épée à deux main'},
    ONE_HANDED_AXE: {icon: 'ra  ra-battered-axe', french_name: 'Hachette'},
    TWO_HANDED_AXE: {icon: 'ra  ra-axe', french_name: 'Hache à deux main'},
    HAMMER: {icon: 'ra  ra-large-hammer', french_name: 'Marteau'},
    SPEAR: {icon: 'ra   ra-trident', french_name: 'Lance'},
    JAVELIN: {icon: 'ra  ra-spear-head', french_name: 'Lance'},
    GUN: {icon: 'ra  ra-revolver', french_name: 'Pistolet'},
    MUSKET: {icon: 'ra   ra-mp5', french_name: 'Fusil'},
}
