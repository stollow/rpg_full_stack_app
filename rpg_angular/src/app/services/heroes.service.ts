import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {HeroesModel} from '../models/heroes';

@Injectable({
  providedIn: 'root'
})

export class HeroesService {

  constructor(private http: HttpClient) { }

  public getHeroes(): Observable<HeroesModel[]> {
    return this.http.get<HeroesModel[]>('http://localhost:8080/heros');
  }

  public getHero(id: string): Observable<HeroesModel> {
    return this.http.get<HeroesModel>('http://localhost:8080/heros/' + id);
  }
}
