package fr.zenika.RapidPeonGenerator.controller;

import fr.zenika.RapidPeonGenerator.Exception.ImpossibleWeaponConfigException;
import fr.zenika.RapidPeonGenerator.models.Hero;
import fr.zenika.RapidPeonGenerator.pojo.HeroRepresentation;
import fr.zenika.RapidPeonGenerator.pojo.NewHeroRepresentation;
import fr.zenika.RapidPeonGenerator.service.HerosServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/heros")
public class HerosController {

    public HerosServices service;
    public HeroRepresentation representation;
    public NewHeroRepresentation newHeroRepresentation;

    @Autowired
    public HerosController(HerosServices service, HeroRepresentation representation, NewHeroRepresentation newHeroRepresentation){
        this.service = service;
        this.representation = representation;
        this.newHeroRepresentation = newHeroRepresentation;
    }

    @GetMapping
    public List<HeroRepresentation> getAll() {
        List<Hero> heroes = this.service.findAll();
        return heroes.stream()
                .map(this.representation::mapHeroToRepresentation)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    ResponseEntity<HeroRepresentation> getOneHero(@PathVariable("id") String id) {
        Optional<Hero> hero = this.service.findById(id);
        return hero
                .map(this.representation::mapHeroToRepresentation)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    ResponseEntity<HeroRepresentation> createHero(@RequestBody(required = false) NewHeroRepresentation body) {
        Optional<Hero> createdHero = null;
        try {
            createdHero = Optional.ofNullable(this.newHeroRepresentation.mapToANewHero(body.getGold(),body.getSexe()));
        } catch (ImpossibleWeaponConfigException e) {
            ResponseEntity.status(500).build();
        }
        return createdHero
                .map(hero -> this.service.save(hero))
                .map(this.representation::mapHeroToRepresentation)
                .map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

}
