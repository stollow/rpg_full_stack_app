package fr.zenika.RapidPeonGenerator.models;

import fr.zenika.RapidPeonGenerator.Exception.ImpossibleWeaponConfigException;

import javax.persistence.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Entity
public class Hero {

    @Id
    private String id;
    private String name;
    private String race;
    private int gold;
    private char sexe;
    @ElementCollection
    @CollectionTable(name = "stats_mapping",
            joinColumns = {@JoinColumn(name = "stat_id", referencedColumnName = "id")})
    @MapKeyColumn(name = "name")
    @Column(name = "stat")
    Map<String,Integer> stats;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="armor_config_id", nullable=true)
    private ArmorConfig armorConfig;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable
            (
                    name="hero_weapons_config",
                    joinColumns={ @JoinColumn(name="hero_id", referencedColumnName="id") },
                    inverseJoinColumns={ @JoinColumn(name="config_id", referencedColumnName="id") }
            )
    private List<WeaponsConfig> weaponsConfig = new ArrayList<>();

    public Hero(String name, String race, int gold, char sexe, int maxStat, Map<String,Integer> stats,List<WeaponsConfig> config,ArmorConfig armorConfig) throws ImpossibleWeaponConfigException {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.race = race;
        this.gold = gold;
        this.stats = stats;
        this.sexe = sexe;
        this.setStats(maxStat);
        this.weaponsConfig = config;
        this.armorConfig = armorConfig;
    }

    public Hero(){ }



    public void setStats(int max){
        int i = 0;
        int result = 0;

        List<String> list = new ArrayList<>(stats.keySet());
        Collections.shuffle(list);

        Map<String, Integer> shuffleMap = new LinkedHashMap<>();
        list.forEach(k->shuffleMap.put(k, stats.get(k)));
        for (Map.Entry<String, Integer> entry : shuffleMap.entrySet()) {
            i++;
            String k = entry.getKey();
            Integer v = entry.getValue();
            if(i<stats.size()) {
                result = ThreadLocalRandom.current().nextInt(max / stats.size()-i - 20/i, max / stats.size()-i + 20*i);
            } else {
                result = max;
            }
            stats.put(k, result);
            max -= result;
        }

    }


    public char getSexe() {
        return sexe;
    }

    public Map<String, Integer> getStats() {
        return stats;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRace() {
        return race;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public ArmorConfig getArmorConfig() {
        return armorConfig;
    }

    public void setArmorConfig(ArmorConfig armorConfig) {
        this.armorConfig = armorConfig;
    }

    public void setWeaponsConfig(WeaponsConfig weaponsConfig) {
        if(weaponsConfig.isPrimary()) {
            this.weaponsConfig.set(0,weaponsConfig);
        }else{
            this.weaponsConfig.set(1,weaponsConfig);
        }
    }

    public List<WeaponsConfig> getWeaponsConfig() {
        return weaponsConfig;
    }
}
