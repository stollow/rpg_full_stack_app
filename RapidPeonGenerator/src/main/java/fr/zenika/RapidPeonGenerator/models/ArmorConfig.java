package fr.zenika.RapidPeonGenerator.models;

import fr.zenika.RapidPeonGenerator.enums.ArmorPiece;
import fr.zenika.RapidPeonGenerator.enums.ArmorTypesEnum;
import fr.zenika.RapidPeonGenerator.enums.RarityEnum;
import fr.zenika.RapidPeonGenerator.models.armorModels.*;
import org.springframework.stereotype.Component;
import utils.MinMax;

import javax.persistence.*;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

@Entity
@Component
public class ArmorConfig {

    @Id
    public String id;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable
            (
                    name="armor_pieces_config",
                    joinColumns={ @JoinColumn(name="armor_config_id", referencedColumnName="id") },
                    inverseJoinColumns={ @JoinColumn(name="piece_id", referencedColumnName="id") }
            )
    Set<Armors> armorsPieces;

    public ArmorConfig(Set<Armors> armorsPieces){
        this.id = UUID.randomUUID().toString();
        this.armorsPieces = armorsPieces;
    }

    public ArmorConfig() {
        this.id = UUID.randomUUID().toString();
    }

    public Armors generateRandomArmor(ArmorPiece piece,ArmorTypesEnum type){
            String material = type.getMaterial();
            MinMax interval = type.getInterval();
            RarityEnum suffixe = RarityEnum.values()[(int) (Math.random() * RarityEnum.values().length)];
            MinMax weigthInterval = type.getWeigthInterval();
            int armor = getArmor(interval, suffixe);
            int weigth = getWeigth(weigthInterval);
            return new Armors(armor,weigth,material,suffixe.name()+" "+type.name()+" "+piece.name(),piece);
    }

    public ArmorConfig generateRandomConfig(){
        ArmorTypesEnum type = ArmorTypesEnum.values()[(int) (Math.random() * ArmorTypesEnum.values().length)];
        Set<Armors> armors = new LinkedHashSet<>();
        EnumSet.allOf(ArmorPiece.class)
                .forEach(piece -> armors.add(generateRandomArmor(piece,type)));
        return new ArmorConfig(armors);
    }

    public int getArmor(MinMax interval, RarityEnum sufffixe){
        int armor = ThreadLocalRandom.current().nextInt(interval.getMin()+sufffixe.getBonus(), interval.getMax()+sufffixe.getBonus());
        return armor;
    }

    public int getWeigth(MinMax interval){
        int weigth = ThreadLocalRandom.current().nextInt(interval.getMin(), interval.getMax());
        return weigth;
    }

    public Set<Armors> getArmorsPieces() {
        return armorsPieces;
    }
}
