package fr.zenika.RapidPeonGenerator.models.weaponsModels;

import fr.zenika.RapidPeonGenerator.enums.WeaponsTypes;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class Weapon {

    @Id
    private String id;
    private int damage;
    private boolean isTwoHanded;
    private float range;
    private String name;
    private WeaponsTypes type;
    private float state;

    public Weapon(){

    }

    public Weapon(int damage, float range, String name, WeaponsTypes type, float state) {
        this.id = UUID.randomUUID().toString();
        this.damage = damage;
        this.range = range;
        this.name = name;
        this.type = type;
        this.state = state;
        this.isTwoHanded = this.type.isTwoHanded();
    }

    public boolean isTwoHanded() {
        return isTwoHanded;
    }

    public String getId() {
        return id;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public float getRange() {
        return range;
    }

    public void setRange(float range) {
        this.range = range;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public WeaponsTypes getType() {
        return type;
    }

    public void setType(WeaponsTypes type) {
        this.type = type;
    }

    public float getState() {
        return state;
    }

    public void setState(float state) {
        this.state = state;
    }
}
