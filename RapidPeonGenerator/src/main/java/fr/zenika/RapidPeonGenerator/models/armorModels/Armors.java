package fr.zenika.RapidPeonGenerator.models.armorModels;

import fr.zenika.RapidPeonGenerator.enums.ArmorPiece;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class Armors {
    @Id
    protected String id;
    protected int armor;
    protected int weigth;
    protected String material;
    protected float state;
    protected String name;
    protected ArmorPiece piece;

    public Armors(){

    }

    public Armors(int armor, int weigth, String material,String name, ArmorPiece piece) {
        this.armor = armor;
        this.weigth = weigth;
        this.material = material;
        this.state = 1f;
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.piece = piece;
    }

    public int getArmor() {
        return armor;
    }

    public void setArmor(int armor) {
        this.armor = armor;
    }

    public int getWeigth() {
        return weigth;
    }

    public void setWeigth(int weigth) {
        this.weigth = weigth;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    public float getState() {
        return state;
    }

    public void setState(float state) {
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArmorPiece getPiece() {
        return piece;
    }
}
