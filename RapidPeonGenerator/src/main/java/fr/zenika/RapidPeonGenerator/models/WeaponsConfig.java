package fr.zenika.RapidPeonGenerator.models;

import fr.zenika.RapidPeonGenerator.Exception.ImpossibleWeaponConfigException;
import fr.zenika.RapidPeonGenerator.enums.WeaponsTypes;
import fr.zenika.RapidPeonGenerator.models.weaponsModels.Shield;
import fr.zenika.RapidPeonGenerator.models.weaponsModels.Weapon;

import javax.persistence.*;
import java.util.*;

@Entity
public class WeaponsConfig{

    @Id
    private String id;
    private int hands = 2;
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Hero hero;
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name="weapons_in_configs")
    private Set<Weapon> weapons = new LinkedHashSet<>();
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "shield_id", referencedColumnName = "id")
    private Shield shield;
    private int ammos = 0;
    private boolean isPrimary;

    public WeaponsConfig(){

    }

    public WeaponsConfig(List<Weapon> weapons, Optional<Shield> shield, boolean isPrimary) throws ImpossibleWeaponConfigException{
        this.id = UUID.randomUUID().toString();
        this.isPrimary = isPrimary;
        for(Weapon weapon : weapons){
            this.setWeaponInConfig(weapon);
        }
        if(shield.isPresent()) {
            this.setShield(shield.get());
        }

    }

    public void setWeaponInConfig(Weapon weapon) throws ImpossibleWeaponConfigException {
        if(this.hands >0){
            if(weapon.getType() == WeaponsTypes.BOW || weapon.getType() == WeaponsTypes.CROSSBOW || weapon.getType() == WeaponsTypes.GUN ||weapon.getType() == WeaponsTypes.MUSKET ){
                this.ammos = 40;
            }
        if(weapon.isTwoHanded()){
            this.hands = 0;
        }else{
            this.hands --;
        }
        weapons.add(weapon);
        }
        else{
            throw new ImpossibleWeaponConfigException("Impossible to create this configuration for weapons");
        }
    }

    public void setShield(Shield shield) throws ImpossibleWeaponConfigException {
        if(this.hands>0){
            this.hands --;
            this.shield = shield;
        }
        else{
            throw new ImpossibleWeaponConfigException("Impossible to create this configuration for weapons");
        }
    }

    public String getId() {
        return id;
    }

    public int getHands() {
        return hands;
    }

    public Set<Weapon> getWeapons() {
        return weapons;
    }

    public Shield getShield() {
        return shield;
    }

    public int getAmmos() {
        return ammos;
    }

    public void setAmmos(int ammos) {
        this.ammos = ammos;
    }

    public boolean isPrimary() {
        return isPrimary;
    }

    public void setPrimary(boolean primary) {
        isPrimary = primary;
    }
}
