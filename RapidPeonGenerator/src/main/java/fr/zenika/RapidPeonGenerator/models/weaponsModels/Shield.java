package fr.zenika.RapidPeonGenerator.models.weaponsModels;

import fr.zenika.RapidPeonGenerator.enums.ShieldWeigthEnum;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.UUID;

@Entity
public class Shield {
    @Id
    private String id;
    private String name;
    private ShieldWeigthEnum weigthType;
    private int weigth;
    private int armor;
    private int damage;
    private int range;
    private int size;
    private float state;

    public Shield(){

    }

    public Shield(ShieldWeigthEnum weigthType, int weigth, int armor, int damage, int range, int size, String name , float state) {
        this.id = UUID.randomUUID().toString();
        this.weigthType = weigthType;
        this.weigth = weigth;
        this.armor = armor;
        this.damage = damage;
        this.range = range;
        this.size = size;
        this.name = name;
        this.state = state;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getState() {
        return state;
    }

    public void setState(float state) {
        this.state = state;
    }

    public ShieldWeigthEnum getWeigthType() {
        return weigthType;
    }

    public int getWeigth() {
        return weigth;
    }

    public int getArmor() {
        return armor;
    }

    public int getDamage() {
        return damage;
    }

    public int getRange() {
        return range;
    }

    public int getSize() {
        return size;
    }
}
