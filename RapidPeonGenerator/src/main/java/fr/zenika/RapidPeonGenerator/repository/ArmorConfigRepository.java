package fr.zenika.RapidPeonGenerator.repository;

import fr.zenika.RapidPeonGenerator.models.ArmorConfig;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface ArmorConfigRepository extends CrudRepository<ArmorConfig,String> {
}
