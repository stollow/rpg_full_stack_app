package fr.zenika.RapidPeonGenerator.repository;

import fr.zenika.RapidPeonGenerator.models.Hero;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface HeroRepository extends CrudRepository<Hero,String> {

    @Query("SELECT h FROM Hero h \n" +
            "JOIN FETCH h.armorConfig ac \n" +
            "JOIN FETCH h.weaponsConfig wc\n" +
            "JOIN FETCH ac.armorsPieces\n" +
            "JOIN FETCH wc.weapons\n" +
            "JOIN FETCH wc.shield\n")
    public Iterable<Hero> findAllAndFetchEagerly();
}
