package fr.zenika.RapidPeonGenerator.enums;

import utils.MinMax;

public enum ArmorTypesEnum {
    MAGICAL("silk", new MinMax(0,5),new MinMax(0,2)),
    LIGHT("cotton",new MinMax(10,15),new MinMax(2,5)),
    MEDIUM("leather",new MinMax(15,25),new MinMax(5,8)),
    MEDIUM_HEAVY("chainmail",new MinMax(25,40),new MinMax(10,20)),
    HEAVY("plate",new MinMax(40,60),new MinMax(20,40));


    private String material;
    private MinMax ArmorInterval;
    private MinMax weigthInterval;

    ArmorTypesEnum(String material, MinMax interval,MinMax weigthInterval)
    {
        this.material = material;
        this.ArmorInterval = interval;
        this.weigthInterval = weigthInterval;
    }

    public String getMaterial() {
        return material;
    }
    public MinMax getInterval(){
        return  this.ArmorInterval;
    }

    public MinMax getWeigthInterval() {
        return weigthInterval;
    }
}
