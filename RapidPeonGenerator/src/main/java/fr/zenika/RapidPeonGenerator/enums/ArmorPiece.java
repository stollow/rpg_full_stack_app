package fr.zenika.RapidPeonGenerator.enums;

public enum ArmorPiece {
    GAUNTLET,
    HELMET,
    BOOTS,
    CHESTPLATE,
}
