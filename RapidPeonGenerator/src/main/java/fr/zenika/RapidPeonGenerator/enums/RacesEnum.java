package fr.zenika.RapidPeonGenerator.enums;

public enum RacesEnum {
    HUMAN,
    TROLL,
    DWARF,
    ORC,
    WOODEN_ELF,
    DARK_ELF,
    GNOME,
    DUCK,
    LIZARDMAN,
    DEMON,
    ANGEL,
    DRYAD
}
