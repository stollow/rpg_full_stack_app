package fr.zenika.RapidPeonGenerator.enums;

import java.util.concurrent.ThreadLocalRandom;

public enum RarityEnum {
    WONDERFULL(20),
    DIRTY(-3),
    USELESS(-8),
    MIGHTY(30),
    BROKEN(-11),
    COMMON(0),
    UNCOMMON(2),
    LEGENDARY(40),
    SCRAPPY(-3),
    DEMONIC(15),
    GODLIKE(45),
    TOURMENTED(-5),
    ANTIC(10),
    BLUNT(-10),
    UNKNOWN(getRandomInt()),
    MYSTIC(25),
    MAGICAL(23),
    NOT_CRAZY(-7),
    SUFFERING(-9),
    ILL(-11),
    PATHETIC(-12),
    ROUGH(-2),
    UGLY(0),
    HORRIBLE(-8),
    BEAUTIFULL(1),
    SICKENING(-4),
    ROTTEN(-6),
    CORRUPTED(-9);

    private int bonus;

    private static int getRandomInt(){
        return ThreadLocalRandom.current().nextInt(-12, 100);
    }

    RarityEnum( int bonus){
        this.bonus = bonus;
    }

    public int getBonus() {
        return bonus;
    }
}
