package fr.zenika.RapidPeonGenerator.enums;

import utils.MinMax;

import java.lang.reflect.Array;

public enum WeaponsTypes {
    BOW(true,30, new MinMax(20,30)),
    CROSSBOW(true,40, new MinMax(20,40)),
    ONE_HANDED_SWORD(false,2, new MinMax(15,25)),
    DAGGER(false,1, new MinMax(12,22)),
    TWO_HANDED_SWORD(true,3, new MinMax(30,50)),
    ONE_HANDED_AXE(false,2, new MinMax(15,25)),
    TWO_HANDED_AXE(true,3, new MinMax(35,55)),
    HAMMER(true,3, new MinMax(40,60)),
    SPEAR(true,5, new MinMax(35,45)),
    JAVELIN(false,25,new MinMax(28,37)),
    GUN(false,15,new MinMax(22,29)),
    MUSKET(true,50,new MinMax(40,55));

    private boolean oneOrTwoHanded;
    private int range;
    private MinMax interval;

    WeaponsTypes (boolean oneOrTwoHanded,int range, MinMax interval) {
        this.oneOrTwoHanded = oneOrTwoHanded;
        this.range = range;
        this.interval = interval;
    }

    public boolean isTwoHanded() {
        return this.oneOrTwoHanded;
    }

    public MinMax getInterval(){
        return this.interval;
    }

    public int getRange(){
        return this.range;
    }
}
