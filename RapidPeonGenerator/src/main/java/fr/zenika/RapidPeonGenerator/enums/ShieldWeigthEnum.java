package fr.zenika.RapidPeonGenerator.enums;

import utils.MinMax;

public enum ShieldWeigthEnum {
    EXTRA_LIGHT(5,new MinMax(12,13)),
    LIGHT(10,new MinMax(13,17)),
    MEDIUM(15,new MinMax(17,23)),
    HEAVY(20,new MinMax(23,30)),
    EXTRA_HEAVY(25,new MinMax(30,40));


    private int weigth;
    private MinMax interval;

    ShieldWeigthEnum (int weigth,MinMax interval) {
        this.interval =interval;
        this.weigth = weigth;
    }

    public int getMaxWeigth(){
        return this.weigth;
    }

    public MinMax getInterval() {
        return interval;
    }
}
