package fr.zenika.RapidPeonGenerator.Exception;

public class ImpossibleWeaponConfigException extends Exception {

    public ImpossibleWeaponConfigException(String message) {
        super(message);
    }
}
