package fr.zenika.RapidPeonGenerator.pojo;
import fr.zenika.RapidPeonGenerator.models.ArmorConfig;
import fr.zenika.RapidPeonGenerator.models.Hero;
import fr.zenika.RapidPeonGenerator.models.WeaponsConfig;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class HeroRepresentation {

    private String id;
    private String name;
    private char sexe;
    private String race;
    private int gold;
    Map<String,Integer> stats;
    private List<WeaponsConfig> config = new ArrayList<>();
    private ArmorConfig armorConfig;

    public HeroRepresentation(String id, String name, char sexe, String race, int gold, Map<String, Integer> stats, List<WeaponsConfig> config, ArmorConfig armorConfig) {
        this.id = id;
        this.name = name;
        this.sexe = sexe;
        this.race = race;
        this.gold = gold;
        this.stats = stats;
        this.config = config;
        this.armorConfig = armorConfig;
    }

    public HeroRepresentation(){}

    public HeroRepresentation mapHeroToRepresentation(Hero h){
        HeroRepresentation representation = new HeroRepresentation(h.getId(),h.getName(),h.getSexe(),h.getRace(),h.getGold(),h.getStats(),h.getWeaponsConfig(),h.getArmorConfig());
        return representation;
    }


    public String getId() {
        return id;
    }

    public Map<String, Integer> getStats() {
        return stats;
    }

    public void setStats(Map<String, Integer> stats) {
        this.stats = stats;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public char getSexe() {
        return sexe;
    }

    public void setSexe(char sexe) {
        this.sexe = sexe;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public int getGold() {
        return gold;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }


    public List<WeaponsConfig> getConfig() {
        return config;
    }

    public ArmorConfig getArmorConfig() {
        return armorConfig;
    }
}
