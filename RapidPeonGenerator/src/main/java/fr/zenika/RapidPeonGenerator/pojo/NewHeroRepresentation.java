package fr.zenika.RapidPeonGenerator.pojo;

import fr.zenika.RapidPeonGenerator.Exception.ImpossibleWeaponConfigException;
import fr.zenika.RapidPeonGenerator.enums.RacesEnum;
import fr.zenika.RapidPeonGenerator.factories.HerosFactory;
import fr.zenika.RapidPeonGenerator.models.Hero;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NewHeroRepresentation {
    private String name;
    private String race;
    private int gold;
    private char sexe;
    HerosFactory factory;

    @Autowired
    public NewHeroRepresentation(HerosFactory factory){
        this.factory = factory;
    }

    public NewHeroRepresentation(String name, String race, int gold, char sexe) {
        this.name = name;
        this.race = race;
        this.gold = gold;
        this.sexe = sexe;
    }

    public Hero mapToANewHero(Integer gold,char sexe) throws ImpossibleWeaponConfigException {
        RacesEnum race = RacesEnum.values()[(int)(Math.random()*RacesEnum.values().length)];
        return this.factory.createHeroe(race.name(),gold,sexe);
    }

    public String getName() {
        return name;
    }

    public String getRace() {
        return race;
    }

    public int getGold() {
        return gold;
    }

    public char getSexe() {
        return sexe;
    }
}
