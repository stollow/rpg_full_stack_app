package fr.zenika.RapidPeonGenerator.service;

import fr.zenika.RapidPeonGenerator.pojo.RandomNameRepresentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RandomNameService {

    private RestTemplate restTemplate;

    @Autowired
    public RandomNameService(RestTemplateBuilder builder){
        this.restTemplate = restTemplate(builder);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    public RandomNameRepresentation getRandomName(char sexe) {
        String url;
        if(sexe == 'f'){
            url = "https://uinames.com/api/?amount=1&region=india&gender=female";
        }else{
            url = "https://uinames.com/api/?amount=1&region=india&gender=male";
        }
            RandomNameRepresentation randomName = this.restTemplate.getForObject(
                    url, RandomNameRepresentation.class);
            return randomName;
    }


}
