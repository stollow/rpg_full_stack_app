package fr.zenika.RapidPeonGenerator.service;

import fr.zenika.RapidPeonGenerator.Exception.ImpossibleWeaponConfigException;
import fr.zenika.RapidPeonGenerator.enums.RarityEnum;
import fr.zenika.RapidPeonGenerator.enums.ShieldWeigthEnum;
import fr.zenika.RapidPeonGenerator.enums.WeaponsTypes;
import fr.zenika.RapidPeonGenerator.models.WeaponsConfig;
import fr.zenika.RapidPeonGenerator.models.weaponsModels.Shield;
import fr.zenika.RapidPeonGenerator.models.weaponsModels.Weapon;
import org.springframework.stereotype.Component;
import utils.MinMax;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

@Component
public class RandomWeaponGenerator {


    public WeaponsTypes getRandomWeapon(){
        WeaponsTypes type = WeaponsTypes.values()[(int)(Math.random()*WeaponsTypes.values().length)];
        return type;
    }

    public ShieldWeigthEnum getShieldWeigth(){
        ShieldWeigthEnum weigth = ShieldWeigthEnum.values()[(int)(Math.random()*ShieldWeigthEnum.values().length)];
        return weigth;
    }

    public RarityEnum getRandomSuffixe(){
        RarityEnum suffixe = RarityEnum.values()[(int)(Math.random()*RarityEnum.values().length)];
        return suffixe;
    }

    public Weapon generateRandomWeapon(){
        WeaponsTypes type = this.getRandomWeapon();
        String typeName = type.name();
        RarityEnum suffixe = this.getRandomSuffixe();
        float state = this.getRandomState();
        return new Weapon(getDamage(type.getInterval(),suffixe),type.getRange(),suffixe+" "+typeName,type,state);
    }

    public int getDamage(MinMax interval,RarityEnum sufffixe){
        int damage = ThreadLocalRandom.current().nextInt(interval.getMin()+sufffixe.getBonus(), interval.getMax()+sufffixe.getBonus());
        return damage;
    }

    public float getRandomState(){
        Random rand = new Random();
        float state = (float) (Math.round(rand.nextDouble() * 100.0) / 100.0);
        return state;
    }

    public WeaponsConfig generateRandomConfig(boolean isPrimary) throws ImpossibleWeaponConfigException {
        Weapon weapon1 = this.generateRandomWeapon();
        if(!weapon1.isTwoHanded()){
            Random rd = new Random();
            WeaponsConfig weaponsConfig = rd.nextBoolean() ? this.generateSecondWeapon(weapon1,isPrimary) : this.generateRandomShield(weapon1,isPrimary);
            return weaponsConfig;
            }
        else{
            WeaponsConfig weaponsConfig = new WeaponsConfig(List.of(weapon1), Optional.empty(),isPrimary);
            return weaponsConfig;
        }
    }

    public WeaponsConfig generateSecondWeapon(Weapon weapon1,boolean isPrimary) throws ImpossibleWeaponConfigException {
        Weapon weapon2 = this.generateRandomWeapon();
        while (weapon2.isTwoHanded()) {
            weapon2 = this.generateRandomWeapon();
        }
        WeaponsConfig weaponsConfig = new WeaponsConfig(List.of(weapon1, weapon2), Optional.empty(), isPrimary);
        return weaponsConfig;
    }

    public WeaponsConfig generateRandomShield(Weapon weapon1, boolean isPrimary) throws ImpossibleWeaponConfigException{
        ShieldWeigthEnum weigth = this.getShieldWeigth();
        RarityEnum suffixe = this.getRandomSuffixe();
        int randomWeith = ThreadLocalRandom.current().nextInt(weigth.getMaxWeigth()-4, weigth.getMaxWeigth());
        float state = this.getRandomState();
        Shield shield = new Shield(weigth,randomWeith,getDamage(weigth.getInterval(),suffixe),getDamage(weigth.getInterval(),suffixe),2,3,suffixe+" "+weigth.name()+" shield",state);
        WeaponsConfig weaponsConfig = new WeaponsConfig(List.of(weapon1), Optional.of(shield), isPrimary);
        return weaponsConfig;
    }


}
