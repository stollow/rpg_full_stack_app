package fr.zenika.RapidPeonGenerator.service;

import fr.zenika.RapidPeonGenerator.models.Hero;
import fr.zenika.RapidPeonGenerator.repository.HeroRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class HerosServices {

    public HeroRepository repository;

    @Autowired
    public HerosServices(HeroRepository repo){
        this.repository = repo;
    }

    public List<Hero>findAll(){
        return (List<Hero>) this.repository.findAll();
    }

    public List<Hero>getAll(){
        return (List<Hero>) this.repository.findAllAndFetchEagerly();
    }

    public Optional<Hero> findById(String id){
        return this.repository.findById(id);
    }

    public Hero save(Hero hero){
        return this.repository.save(hero);
    }
}
