package fr.zenika.RapidPeonGenerator.factories;

import fr.zenika.RapidPeonGenerator.Exception.ImpossibleWeaponConfigException;
import fr.zenika.RapidPeonGenerator.enums.StatsEnum;
import fr.zenika.RapidPeonGenerator.models.ArmorConfig;
import fr.zenika.RapidPeonGenerator.models.Hero;
import fr.zenika.RapidPeonGenerator.models.WeaponsConfig;
import fr.zenika.RapidPeonGenerator.repository.HeroRepository;
import fr.zenika.RapidPeonGenerator.service.RandomNameService;
import fr.zenika.RapidPeonGenerator.service.RandomWeaponGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class HerosFactory {

    public HeroRepository repository;
    public RandomNameService randomNameService;
    public RandomWeaponGenerator randomWeaponGenerator;
    public ArmorConfig armorConfig;


    @Autowired
    public HerosFactory(HeroRepository repo, RandomNameService randomNameService, RandomWeaponGenerator randomWeaponGenerator, ArmorConfig armorConfig){
        this.repository = repo;
        this.randomNameService = randomNameService;
        this.randomWeaponGenerator = randomWeaponGenerator;
        this.armorConfig = armorConfig;
    }

    public HerosFactory(){}



    public Hero createHeroe( String race, int gold,char sexe) throws ImpossibleWeaponConfigException {
        Map<String,Integer> stats = new HashMap<>();
        for(StatsEnum stat : StatsEnum.values()){
            stats.put(stat.getName(),0);
        }
        WeaponsConfig config = this.randomWeaponGenerator.generateRandomConfig(true);
        WeaponsConfig config2 = this.randomWeaponGenerator.generateRandomConfig(false);
        List<WeaponsConfig> configs = List.of(config,config2);
        Hero hero = new Hero(this.randomNameService.getRandomName(sexe).getName()+" "+this.randomNameService.getRandomName(sexe).getSurname(),race,gold,sexe,300,stats,configs,armorConfig.generateRandomConfig());
        repository.save(hero);
        return hero;
    }

}
