package fr.zenika.RapidPeonGenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RapidPeonGeneratorApplication{

	public static void main(String[] args) {
		SpringApplication.run(RapidPeonGeneratorApplication.class, args);
	}

}
