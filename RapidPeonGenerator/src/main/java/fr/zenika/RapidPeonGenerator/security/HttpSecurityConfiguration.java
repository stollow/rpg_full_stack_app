package fr.zenika.RapidPeonGenerator.security;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
public class HttpSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http
                .httpBasic().and()
                .authorizeRequests()
                .antMatchers("/heros" , "/heros/**")
                .permitAll().anyRequest().authenticated()
                .and().csrf().disable();
    }
}
